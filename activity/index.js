
// PART1

function resultOfAddition(numA, numB){
    console.log("Displayed sum of " + numA + " of " + numB);
    console.log(numA + numB);
};

resultOfAddition(5, 15);

function resultOfSubtraction(numC, numD){
    console.log("Displayed difference of " + numC + " of " + numD);
    console.log(numC - numD);
};

resultOfSubtraction(20, 5);

// PART 2

function resultOfMultiplication(numE, numF){
    console.log("The product of " + numE + " and " + numF);
    let formula = numE * numF;
    return formula;
};

let product = resultOfMultiplication(50, 10);
console.log(product);

function resultOfDivision(numE, numF){
    console.log("The quotient of " + numE + " and " + numF);
    let formula =  numE / numF;
    return formula;
};

let quotient = resultOfDivision(50, 10);
console.log(quotient);

// PART 3

function circleArea(radius){
    let pie = 3.14
    let formula = pie * (radius**2);
    console.log("The result of getting the area of a circle with " + radius + " radius:")
    return formula;
}

let areaResult = circleArea(15);
console.log(areaResult);


// PART 4

function average(numberA, numberB, numberC, numberD){
    let formula = (numberA + numberB + numberC + numberD) / 4
    console.log("The average of " + numberA + ", " + numberB + ", " + numberC + ", " + numberD + ":");
    return formula;
}

let averageVar = average(20, 40, 60, 80);
console.log(averageVar);
	

// PART 5

function passingGrade(yourScore, totalScore){
    let percentage = (yourScore / totalScore) * 100;
    let passingScore = 75
    let isPassed = percentage >= passingScore;
    console.log("Is " + yourScore + "/" + totalScore + " a passing score?")
    return isPassed;
}

let isPassingScore = passingGrade(38, 50);
console.log(isPassingScore);