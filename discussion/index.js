console.log("S18 Discussion");

// function printInfo(){
//     let nickname = prompt("Enter your nickname: ");
//     console.log("Hi, " + nickname);
// };

// printInfo();
//function declaration //function name// parameter //codeblock
function printName(name){
    console.log("My name is " + name);
}
//invocation //argument
printName("Joana");
printName("John");

// Parameter - acts as named variable/container that exists only inside a function

let sampleVariable = "Inday";

printName(sampleVariable);
// Variables can also be passed as an argument

function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log("The remainder of " + num + " divided by 8 is: " + remainder);
    let isDivisibilityby8 = remainder === 0;
    console.log("Is " +num+ " divisible by 8?");
    console.log(isDivisibilityby8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Function as argument
    // Some complex functions uses other functions to perform more complicated results

function argumentFunction(){
    console.log("This function was passed as an argument before the message was printed.");
}

function invokeFunction(argumentFunction){
    argumentFunction();
}

invokeFunction(argumentFunction);

function createFullname(firstName, middleName, lastName){
    console.log("My full name is " + firstName +" "+ middleName+" " + lastName);
}

createFullname("Jesus", "Aquino", "Carullo");

// Using variables as an argument

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullname(firstName, middleName, lastName)

function getDifferenceOf8Minus4(numA, numB){
    console.log("Difference "+ (numA - numB))
}

getDifferenceOf8Minus4(8,4);

// The return statement allows us to output a value from a function to be passed to the line/block of code that invoke/called

function returnFullName(firstName, middleName, lastName){
    return firstName + " " + middleName + " " + lastName;
    // codes after return does not execute
}

let completeName = returnFullName("Jeffrey", "Smith" , "Jordan");
console.log(completeName);

console.log("I am " + completeName);

function printPlayerInfo(username, level, job){
     console.log("Username: " + username),
     console.log("Level: " + level),
     console.log("Job: " + job)

     return username + level + job
};

let user1 = printPlayerInfo("boxzMapagmahal" , "Senior" , "Programmer");


console.log(user1); 


